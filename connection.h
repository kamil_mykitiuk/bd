//
// Created by Kamil Mykitiuk on 29/01/2018.
//

#ifndef BD_CONNECTION_H
#define BD_CONNECTION_H

#include <pqxx/pqxx>
#include <vector>
#include <string>

void trans_query(const std::vector<std::string>& query);

pqxx::result non_trans_query(const std::string& query);

void prepare_connstring();

std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>
transform_result(const pqxx::result& query_to_print, std::vector<std::string>& titles);

std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>
transform_result(const pqxx::result& query_to_print);

#endif //BD_CONNECTION_H
