/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "menu.h"
#include "actions.h"
#include "state.h"
#include "connection.h"

using namespace std;

int main(void) {
    prepare_connstring();
    State state;

    Menu main_menu;
    main_menu.add_action("[O]twórz Zamówienie\n", "O",  make_shared<ChangeStateAction>(State::ORDER));
    main_menu.add_action("[D]ostawa Składników\n", "D",  make_shared<AddIngrendients>());
    main_menu.add_action("[S]tan Magazynu\n", "S", make_shared<DisplayIngrendients>());
    main_menu.add_action("[Z]akończ\n", "Z", make_shared<ChangeStateAction>(State::END));
    main_menu.add_action("[W]ypisz rachunki", "W", make_shared<DisplayRecipitAction>());

    Menu order_menu;
    order_menu.add_action("[D]odaj pizze\n", "D", make_shared<SetPizzaMenu>());
    order_menu.add_action("[U]sun pizze\n", "U", make_shared<SetRemoveMenu>());
    order_menu.add_action("[P]okaż rachunek\n", "P", make_shared<DisplayOrderAction>());
    order_menu.add_action("[A]nuluj transakcje\n", "A", make_shared<ResetOrderAction>());
    order_menu.add_action("[Z]atwierdź zamównienie", "Z", make_shared<AcceptOrderAction>());
    state.add_menu(main_menu, State::MAIN);
    state.add_menu(order_menu, State::ORDER);
    InputAction inputAction;

    cout<<"Witaj w pizzerii BD. Obsługa menu polega na wpsiywaniu fraz ograniczononcyh [ ]. Smacznego!\n";
    while (state) {
        inputAction.execute(state);
    }

    return 0;
}
