//
// Created by Kamil Mykitiuk on 29/01/2018.
//

#include "pizza.h"

std::string Pizza::get_name() const {
    return _name;
}

double Pizza::get_price() const {
    return _price;
}

Pizza::Pizza(const std::string& _name, double _price, int _id) : _name(_name),
                                                                 _price(_price),
                                                                 _id(_id) {}

int Pizza::get_id() const {
    return _id;
}
