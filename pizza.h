//
// Created by Kamil Mykitiuk on 29/01/2018.
//

#ifndef BD_PIZZA_H
#define BD_PIZZA_H

#include <string>

class Pizza {
    std::string _name;
    double _price;
    int _id;

public:

    Pizza(const std::string& _name, double _price, int _id);

    std::string get_name() const;

    double get_price() const;

    int get_id() const;

    bool operator<(const Pizza& other) const {
        return this->_name < other._name;
    }
};

#endif //BD_PIZZA_H
