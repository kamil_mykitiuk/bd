#include "connection.h"
#include <fstream>
#include <iostream>
#include "printer.h"

std::string connstring;

void trans_query(const std::vector<std::string>& querries) {
    pqxx::connection tconn_t(connstring);
    pqxx::work twerk(tconn_t);
    for (const auto& query: querries) {
        twerk.exec(query);
    }
    twerk.commit();
    tconn_t.disconnect();
}

pqxx::result non_trans_query(const std::string& query) {
    pqxx::connection tconn_nt(connstring);
    pqxx::nontransaction nt(tconn_nt);
    pqxx::result tq(nt.exec(query));
    tconn_nt.disconnect();
    return tq;
}

std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>
transform_result(const pqxx::result& query_to_print) {
    std::vector<std::vector<std::string>> query_result_vector;
    std::vector<size_t> column_width;
    std::vector<std::string> v_temp;
    bool first_row = true;
    for (auto row = query_to_print.begin(); row != query_to_print.end(); ++row) {
        v_temp.clear();
        for (auto field = row->begin(); field != row->end(); ++field) {
            //Add spaces around each side of the value for printing.
            std::string temp = " " + std::string(field->c_str()) + " ";
            v_temp.push_back(temp);

            auto it_pos = std::distance(row->begin(), field);
            if (first_row) column_width.push_back(temp.size());
            column_width[it_pos] = std::max(column_width[it_pos], temp.size());


        }
        first_row = false;
        query_result_vector.push_back(v_temp);
    }
    return make_pair(query_result_vector, column_width);
};


std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>
transform_result(const pqxx::result& query_to_print, std::vector<std::string>& titles) {
    std::vector<size_t> column_width;
    std::vector<std::vector<std::string>> query_result_vector;
    std::vector<std::string> v_temp;
    for (const auto& title: titles) {
        v_temp.push_back(" " + title + " ");
        column_width.push_back(title.size() + 2);
    }
    query_result_vector.push_back(v_temp);

    for (auto row = query_to_print.begin(); row != query_to_print.end(); ++row) {
        v_temp.clear();
        for (auto field = row->begin(); field != row->end(); ++field) {
            //Add spaces around each side of the value for printing.
            std::string temp = " " + std::string(field->c_str()) + " ";
            v_temp.push_back(temp);

            auto it_pos = std::distance(row->begin(), field);
            column_width[it_pos] = std::max(column_width[it_pos], temp.size());


        }
        query_result_vector.push_back(v_temp);
    }
    return make_pair(query_result_vector, column_width);
}

void prepare_connstring() {
    std::ifstream conf_file("pqxxconn.conf");
    if (conf_file.is_open()) {
        std::getline(conf_file, connstring);
    } else {
        pstring_n("Could not open 'pqxxconn.conf'.");
    }
    conf_file.close();
    pqxx::connection tconn(connstring);
    if (tconn.is_open()) {
        std::cout << "Successfully connected to: " << tconn.dbname() << std::endl;
    } else {
        pstring_n("Can't open database.");
        exit(1);
    }
    tconn.disconnect();
}