#ifndef BD_STATE_H
#define BD_STATE_H

#include <set>
#include <map>

#include "menu.h"
#include "pizza.h"

class Menu;

class State {
public:
    enum State_enum {
        MAIN, ORDER, PIZZA, END, REMOVE
    };

private:

    State_enum _state;

    std::map<Pizza, int> _order;

    std::map<State_enum, Menu> _menus;

public:

    State() : _state(MAIN) {}

    void change_state(State_enum s);

    void add_pizza(const Pizza& p);

    void remove_pizza(const Pizza& p);

    void reset_order();

    void add_menu(const Menu& menu, State_enum);

    Menu get_menu();

    std::map<Pizza, int> get_order();

    explicit operator bool() const;
};




#endif //BD_STATE_H
