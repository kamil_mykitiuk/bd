#ifndef BD_PRINTER_H
#define BD_PRINTER_H

void print_table(std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>&&);

void pstring_n(const std::string&);

#endif //BD_PRINTER_H
