//
// Created by Kamil Mykitiuk on 22/01/2018.
//

#ifndef BD_MENU_H
#define BD_MENU_H

#include <string>
#include <utility>
#include <vector>
#include <memory>

class Action;

struct Binding {
    std::string _description;
    std::shared_ptr<Action> _action;
    std::string _key;

    Binding(std::string _description, std::shared_ptr<Action>,
                     std::string key);
};


class Menu {
    std::string _header = "Wybierz opcje:\n";

    std::vector<Binding> _options;
public:
    Menu() = default;

    explicit Menu(std::vector<Binding>& options) : _options(options) {}

    Menu(std::string header) : _header(std::move(header)) {}

    void show();

    Action& select();

    void add_action(const std::string& desc, const std::string& opt,
                    const std::shared_ptr<Action>& action);

    void add_action(const std::string& desc, const std::string& opt,
                    std::shared_ptr<Action>&& action);

};


#endif //BD_MENU_H
