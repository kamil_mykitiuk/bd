#include <string>
#include <iostream>
#include <vector>
#include "printer.h"

void pstring(const std::string& s) {
    std::cout << s;
}

//function to print single std::string, with newline.
void pstring_n(const std::string& s) {
    std::cout << s << "\n";
}

std::string print_repeat(const std::string& s, const size_t& size) {
    std::string temp = "";
    for (size_t i = 0; i < size; i++) {
        temp += s;
    }
    return temp;
}

void print_table(std::pair<std::vector<std::vector<std::string>>, std::vector<unsigned long>>&& table) {
    auto& column_width = table.second;
    auto& query_result_vector = table.first;
    std::string divider = "";
    size_t m = column_width.size() - 1;

    for (size_t j = 0; j < column_width.size(); j++) {
        divider += print_repeat("-", column_width[j]);
        if (j != m) { divider += "+"; }
    }

    size_t k = query_result_vector.size() -1;

    for (size_t d = 0; d < query_result_vector.size(); d++) {
        for (size_t i = 0; i < column_width.size(); i++) {
            std::cout << query_result_vector[d][i] << print_repeat(" ", column_width[i] - query_result_vector[d][i].size());
            if (i != m) { pstring("|"); }
        }
        printf("\n");
        if (d != k) { pstring_n(divider); }
    }
}