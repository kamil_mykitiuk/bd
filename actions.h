//
// Created by Kamil Mykitiuk on 22/01/2018.
//

#ifndef BD_ACTIONS_H
#define BD_ACTIONS_H

#include <set>
#include <map>
#include <utility>
#include "state.h"

class Pizza;

class Action {

public:
    virtual State& execute(State& state) = 0;
};

class ChangeStateAction : public Action {
private:
    State::State_enum state_enum;
public:
    explicit ChangeStateAction(State::State_enum state_enum) :
            state_enum(state_enum) {}

    State& execute(State& state) override {
        state.change_state(state_enum);
        return state;
    }
};

class AddPizza : public Action {
private:
    Pizza pizza;
public:
    explicit AddPizza(Pizza pizza) : pizza(std::move(pizza)) {}

    State& execute(State& state) override;
};


class RemovePizza : public Action {
private:
    Pizza pizza;
public:
    explicit RemovePizza(Pizza pizza) : pizza(std::move(pizza)) {}

    State& execute(State& state) override;
};

class ResetOrderAction : public Action {
public:
    State& execute(State& state) override {
        state.reset_order();
        state.change_state(State::MAIN);
        return state;
    }
};

class AddIngrendients : public Action {
public:
    State& execute(State& state) override;
};

class InputAction : public Action {
public:
    State& execute(State& state) override;
};

class AcceptOrderAction : public Action {

    State& execute(State& state) override;

};

class DisplayOrderAction : public Action {

public:
    State& execute(State& state) override;

};

class DisplayIngrendients : public Action {
public:
    State& execute(State& state) override;
};

class SetPizzaMenu : public Action {
public:
    State& execute(State& state) override;
};

class SetRemoveMenu : public Action {
public:
    State& execute(State& state) override;
};

class DisplayRecipitAction : public Action {
public:
    State& execute(State& state) override;
};

#endif //BD_ACTIONS_H
