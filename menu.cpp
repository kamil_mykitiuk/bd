#include "menu.h"
#include "actions.h"
#include <iostream>
#include <utility>

using namespace std;

string unknown = "Nienzana opcja\n";

void Menu::show() {
    cout << this->_header;
    for (auto& i: this->_options) {
        cout << i._description;
    }
    cout<<"\n";
}

Action& Menu::select() {
    while (true) {
        std::string op;
        cin >> op;
        for (auto& i: this->_options) {
            if (op == i._key) {
                return i._action.operator*();
            }
        }
        cout << unknown;
    }
}

void Menu::add_action(const std::string& desc, const std::string& opt, const std::shared_ptr<Action>& action) {
    this->_options.emplace_back(Binding(desc, action, opt));
}

void Menu::add_action(const std::string& desc, const std::string& opt, std::shared_ptr<Action>&& action) {
    this->_options.emplace_back(Binding(desc, action, opt));
}

Binding::Binding(string _description, std::shared_ptr<Action> _action,
                 std::string key) : _description(std::move(_description)), _action(std::move(_action)),
                                 _key(std::move(key)) {}