#include "state.h"
#include "pizza.h"

#include <map>
#include <iostream>


State::operator bool() const {
    return _state != END;
}

void State::add_pizza(const Pizza& p) {
    _order[p]++;
}

void State::change_state(State_enum s) {
    _state = s;
}

void State::remove_pizza(const Pizza& p) {
    _order[p]--;
}

void State::add_menu(const Menu& menu, State::State_enum state_enum) {
    _menus[state_enum] = menu;
}

std::map<Pizza, int> State::get_order() {
    return _order;
}

Menu State::get_menu() {
    return _menus[_state];
}

void State::reset_order() {
    _order.clear();
}
