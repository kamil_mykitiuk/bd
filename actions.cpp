
#include "actions.h"
#include "menu.h"
#include "connection.h"
#include "printer.h"
#include <iostream>
#include <algorithm>
#include <uuid/uuid.h>

State& InputAction::execute(State& state) {
    state.get_menu().show();
    Action& a = state.get_menu().select();
    return a.execute(state);
}

State& AcceptOrderAction::execute(State& state) {
    uuid_t uuid;
    uuid_generate(uuid);
    char uuid_s[37];
    uuid_unparse(uuid, uuid_s);
    std::vector<std::string> queries;
    queries.push_back("INSERT INTO rachunek(id_rachunku) VALUES('" + std::string(uuid_s) + "');");
    for (auto i: state.get_order()) {
        if (i.second > 0) {
            queries.push_back("INSERT INTO pozycja(ilość, pizza_id_pizzy, rachunek_id_rachunku) VALUES("
                              + std::to_string(i.second) + ", "
                              + std::to_string(i.first.get_id()) + ", '"
                              + std::string(uuid_s) + "');");
        }
    }
    try {
        trans_query(queries);
    } catch (pqxx::plpgsql_raise error) {
        std::cout << "Zamównie nie powiodło się z powodu: brak składnikow\n";
    }
    std::cout << "Transakcja zakończona\n";

    return ResetOrderAction().execute(state);
}

State& DisplayOrderAction::execute(State& state) {
    std::cout << "Nazwa: ilość cena\n";
    double sum = 0;
    std::vector<std::vector<std::string>> data;
    std::vector<unsigned long> column_width;
    std::vector<std::string> temp_v;

    temp_v.emplace_back(" Nazwa ");
    column_width.push_back(temp_v.back().length());
    temp_v.emplace_back(" Ilosc ");
    column_width.push_back(temp_v.back().length());
    temp_v.emplace_back(" Cena ");
    column_width.push_back(temp_v.back().length());
    data.push_back(temp_v);

    for (auto& i: state.get_order()) {
        if (i.second > 0) {
            temp_v.clear();
            temp_v.push_back(" " + i.first.get_name() + " ");
            column_width[0] = std::max(column_width[0], temp_v.back().length());

            temp_v.push_back(" " + std::to_string(i.second) + " ");
            column_width[1] = std::max(column_width[1], temp_v.back().length());

            temp_v.push_back(" " + std::to_string(i.second * i.first.get_price()) + " ");
            column_width[2] = std::max(column_width[2], temp_v.back().length());
            sum += i.second * i.first.get_price();
            data.push_back(temp_v);
        }
    }
    print_table(std::make_pair(data, column_width));
    std::cout << "\n Razem: " << sum << "\n";
    return state;
}

State& DisplayIngrendients::execute(State& state) {
    static auto titles = std::vector<std::string> {"Nazwa", "Ilosc"};
    print_table(transform_result(non_trans_query("SELECT nazwa, zapas FROM składnik;"), titles));
    return state;
}

State& SetPizzaMenu::execute(State& state) {
    Menu pizza_menu("Wybierz pizze: \n");
    auto result = non_trans_query("SELECT nazwa, cena, id_pizzy FROM pizza;");
    for (auto row: result) {
        auto name = row.begin();
        auto cena = name + 1;
        auto id_pizzy = cena + 1;
        Pizza pizza(name.c_str(), std::stod(cena.c_str()), std::stoi(std::string(id_pizzy.c_str())));
        pizza_menu.add_action("[" + std::string(name.c_str()) + "]\n", name.c_str(), std::make_shared<AddPizza>(pizza));
    }
    pizza_menu.add_action("[P]owrot", "P", std::make_shared<ChangeStateAction>(State::ORDER));
    state.add_menu(pizza_menu, State::PIZZA);
    state.change_state(State::PIZZA);
    return state;
}

State& AddPizza::execute(State& state) {
    std::cout << "Pizza " << pizza.get_name() << " dodana do zamówienia.\n";
    state.add_pizza(pizza);
    return DisplayOrderAction().execute(state);
}

State& SetRemoveMenu::execute(State& state) {
    Menu pizza_menu("Wybierz pizze: \n");
    for (auto& i: state.get_order()) {
        if (i.second > 0) {
            pizza_menu.add_action("[" + i.first.get_name() + "]\n", i.first.get_name(),
                                  std::make_shared<RemovePizza>(i.first));
        }
    }
    pizza_menu.add_action("[P]owrot", "P", std::make_shared<ChangeStateAction>(State::ORDER));
    state.add_menu(pizza_menu, State::REMOVE);
    state.change_state(State::REMOVE);
    return state;
}

State& RemovePizza::execute(State& state) {
    state.remove_pizza(pizza);
    if (state.get_order()[pizza] == 0)
        state = SetRemoveMenu().execute(state);
    return state;
}

State& AddIngrendients::execute(State& state) {
    std::vector<std::string> queries;
    queries.emplace_back("UPDATE składnik SET zapas=zapas+5;");
    trans_query(queries);
    std::cout << "Uzupełniono stan magazynu.\n";
    return DisplayIngrendients().execute(state);
}

State& DisplayRecipitAction::execute(State& state) {
    std::vector<std::string> temp_v;
    temp_v.emplace_back("Data");
    temp_v.emplace_back("Wartosc");
    print_table(transform_result(non_trans_query(
            "SELECT data, SUM(kol) "
                    "FROM ("
                    "SELECT data, kol FROM rachunek LEFT JOIN "
                    "(SELECT rachunek_id_rachunku, ilość * cena AS kol FROM pozycja LEFT JOIN"
                    " pizza ON pozycja.pizza_id_pizzy = id_pizzy) AS p "
                    "ON id_rachunku = p.rachunek_id_rachunku"
                    ") as q "
                    "GROUP BY data;"), temp_v));
    return state;
}
